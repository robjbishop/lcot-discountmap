const fs = require('fs');
const StringBuilder = require('node-stringbuilder');

var tmpl = fs.readFileSync('marker-num-tmpl.svg', 'utf-8');
var geo = JSON.parse(fs.readFileSync('data/geo.json', 'utf-8'));
var data = geo.features;

function getMarkers() {
    var markers = [];
    for (var i = 0; i < data.length; i++) {
        var marker = tmpl
            .replace("layer1-", "layer1-" + i)
            .replace("layer2-", "layer2-" + i)
            .replace("svg_1-", "svg_1-" + i)
            .replace("svg_2-", "svg_2-" + i)
            .replace("svg_3-", "svg_3-" + i)
            .replace("svg_4-", "svg_4-" + i)
        markers.push(marker);
    }
    return markers;
}

function buildMarkerSvg() {
    var markers = getMarkers();
    for (var i = 0; i < data.length; i++) {

        var testOffSet = 0;
        var textXadj = i + testOffSet > 8 ? -1.75 : -1;
        var textYadj = i + testOffSet > 8 ? 1.25 : 1.25;

        var x = parseFloat(data[i].properties.print_x);
        var y = parseFloat(data[i].properties.print_y);
        console.log(`${(i+1)} ${data[i].properties.name} (${data[i].properties.print_x},${data[i].properties.print_y})`);
        markers[i] = markers[i]
            .replace(/x-coord1/g, x)
            .replace(/y-coord1/g, y)
            .replace(/x-coord2/g, x + textXadj)
            .replace(/y-coord2/g, y + textYadj)
            .replace("r_1", "2.75")
            .replace("r_2", "2.25")
            .replace("f_size", "3.5px")
            .replace("stroke_width", "0.25")
            .replace("_index_", i + 1 + testOffSet);
    }
    var base = fs.readFileSync('base.svg', 'utf-8');
    var newSvg = base.replace("<!-- insert here -->", markers.join(''));
    fs.writeFileSync('build/map-overlay.svg', newSvg, 'utf-8');
}

function buildMarkerHtml() {
    var sb1 = new StringBuilder();
    var markers = getMarkers();
    var table1Html, table2Html;
    for (var i = 0; i < data.length; i++) {
        
        var testOffSet = 0;
        var textXadj = i + testOffSet > 8 ? -5 : 0;
        var textYadj = i + testOffSet > 8 ? 0 : 0;

        var svg = markers[i]
            .replace(/x-coord1/g, 16)
            .replace(/y-coord1/g, 15)
            .replace(/x-coord2/g, 11 + textXadj)
            .replace(/y-coord2/g, 21 + textYadj)
            .replace("r_1", "15")
            .replace("r_2", "13")
            .replace("f_size", "18px")
            .replace("stroke_width", "0.87")
            .replace("_index_", i + 1 + testOffSet);
        
        sb1.append("\n<tr>");
        sb1.append("<td>\n");
        sb1.append("<svg version='1.1' style='background-color:none;padding-left:1;padding-top:1' width='33' height='33' id='svg_"+i+"'>"); 
        sb1.append(svg);
        sb1.append("</svg>\n");
        sb1.append("</td>");
        sb1.append("<td>"+data[i].properties.name+(data[i].properties.print_x ? "": "<sup>1</sup>")+"</td>");
        sb1.append("</tr>");

        if (i === 15) {
            table1Html = sb1.toString();
            sb1.clear();
        }
    }
    table2Html = sb1.toString();
    var base = fs.readFileSync('legend-base.html', 'utf-8');
    var document = base.replace("<!-- table-html -->", table1Html);
    document = document.replace("<!-- table-html -->", table2Html);
    document = document.replace('<!-- kml-datetime -->', fs.statSync('./data/ChamberOfTade.kml').mtime.toISOString());
    fs.writeFileSync('build/legend.html', document, 'utf-8');
}

buildMarkerSvg();
buildMarkerHtml();